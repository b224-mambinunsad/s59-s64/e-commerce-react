const categoriesData = [
    {
        title: "Mobiles",
        img: "https://lzd-img-global.slatic.net/g/p/22159f8059adcbf9de0690464b5a537b.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Men's T-Shirts",
        img: "https://lzd-img-global.slatic.net/g/p/9ac04f1c0265f4ca6a544cee2f486a9b.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Women's Dresses",
        img: "https://lzd-img-global.slatic.net/g/p/bfe31765ba193b2e253d21cd508dcff2.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Sandals",
        img: "https://lzd-img-global.slatic.net/g/p/0506461563b263390f17fe34db8e39bb.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Men Shoes",
        img: "https://lzd-img-global.slatic.net/g/p/c1177b996e55b19cef94d649233f36b5.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Water Bottles",
        img: "https://lzd-img-global.slatic.net/g/p/f324f2a2ce59f5d7699c201cb1fcc94f.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Men Pants",
        img: "https://lzd-img-global.slatic.net/g/p/864e4f985ad939500ae4c5046d10e58d.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Mobile Phone Cases",
        img: "https://lzd-img-global.slatic.net/g/p/535438e981e7a4e3ec8023adb7b1ab41.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Headphones",
        img: "https://lzd-img-global.slatic.net/g/p/e23ab72ee207646aa3e2815bd6752a4f.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Electrical Parts",
        img: "https://lzd-img-global.slatic.net/live/ph/p/b2d48bb01c2c2a96ed9fa0029de25534.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Cables",
        img: "https://lzd-img-global.slatic.net/g/p/c718166849291f991e92ce482f1731a3.png_80x80q80.jpg_.webp"
    },
    {
        title: "Bags",
        img: "	https://lzd-img-global.slatic.net/live/ph/p/a4ac1c945984eb02fdf7dc3766f50594.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Women Shoes",
        img: "https://lzd-img-global.slatic.net/g/p/a9d353ffc5d3f5d4be207af76081af83.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Makeups",
        img: "https://lzd-img-global.slatic.net/g/p/22342d12800b1ac9b93e420883e45f44.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Women Pants",
        img: "https://lzd-img-global.slatic.net/g/p/f6a81d731bdcb69a8cbf8e877d2937f0.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Shorts",
        img: "https://lzd-img-global.slatic.net/g/p/16cd1d95ffb499c1b20c64e486549271.jpg_80x80q80.jpg_.webp"
    },
    {
        title: "Babies",
        img: "https://cf.shopee.ph/file/fbf1d6707dac5f09db46e60b87aae351_tn"
    },
    {
        title: "Toys",
        img: "https://cf.shopee.ph/file/aff648bd1cc6d00de3d908457de3e128_tn"
    },
    {
        title: "Helmet",
        img: "https://cf.shopee.ph/file/34e01853a846ee2280853aa6792effca_tn"
    },
    {
        title: "Computer",
        img: "https://cf.shopee.ph/file/25936be031f917470cb8c8ad9f311413_tn"
    }
]

export default categoriesData;