import React from 'react';

const RenderContext = React.createContext();

export const RenderProvider = RenderContext.Provider;

export default RenderContext;