import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import './App.css';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import UserDetails from './pages/UserDetails';
import UserOrders from './pages/UserOrders';
import Error from './pages/Error';
import Contact from './pages/Contact';
import Products from './pages/Products';
import ProductsCategory from './pages/ProductsCategory';
import ProductView from './pages/ProductView';
import Admin from './pages/Admin';
import AllProducts from './pages/AllProducts';
import AddProduct from './pages/AddProduct';
import AllOrders from './pages/AllOrders';
import SetRole from './pages/SetRole';
import SetShopName from './pages/SetShopName';
import AllUserDetails from './pages/AllUserDetails';
import { UserProvider } from './UserContext';
import { RenderProvider } from './RenderContext';
import { CategoryProvider } from './CategoryContext';



function App() {

  const [user, setUser] = useState({
    id: null,
    name: null,
    email: null,
    shopName: null,
    role: null
  });

  const [toRender, setToRender] = useState(false);

  const [category, setCategory] = useState("");

  const unsetUser = () => {
    localStorage.clear()
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          name: data.name,
          email: data.email,
          shopName: data.shopName,
          role: data.role
        })
      }else{
        setUser({
          id: null,
          name: null,
          email: null,
          shopName: null,
          role: null
        })
      }
    })
    .catch(error => console.log(error))
  }, []);

  return(
    <UserProvider value={{user, setUser, unsetUser}}>
      <RenderProvider value={{toRender, setToRender}}>
        <CategoryProvider value={{category, setCategory}}>
          <Router>
              <div className='main'>
                <AppNavBar />
                <Container >
                    <Routes>
                      <Route path="/" element={<Home />} /> 
                      <Route path="/*" element={<Error />} /> 
                      <Route path="/register" element={<Register />} /> 
                      <Route path="/login" element={<Login />} /> 
                      <Route path="/logout" element={<Logout />} />
                      <Route path="/userDetails" element={<UserDetails />} /> 
                      <Route path="/userOrders" element={<UserOrders />} /> 
                      <Route path="/contact" element={<Contact />} />
                      <Route path='/products' element={<Products />} />
                      <Route path='/products/category' element={<ProductsCategory />} />
                      <Route path='products/:productId' element={<ProductView />} />
                      {(user.role === "Admin" || user.role === "Seller" ?
                        <>
                          <Route path='/admin' element={<Admin />} />
                          <Route path='/admin/allProducts' element={<AllProducts />} />
                          <Route path='/admin/addProduct' element={<AddProduct />} />
                          <Route path='/admin/allOrders' element={<AllOrders />} />
                          <Route path='/admin/setRole' element={<SetRole />} />
                          <Route path='/admin/setShopName' element={<SetShopName />} />
                          <Route path='/admin/allUserDetails' element={<AllUserDetails />} />
                        </>
                      :
                        <Route path="/*" element={<Error />} /> 
                      )}
                    </Routes>
                </Container>
              </div>
          </Router>
        </CategoryProvider>
      </RenderProvider>
    </UserProvider>
  )
};

export default App;
