import { useContext, useState, useEffect } from "react";
import { Table, Row, Col,Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import AllOrdersTable from "../components/AllOrdersTable";
import UserContext from "../UserContext";
import RenderProvider from '../RenderContext';



export default function UserOrders(){

    const { user } = useContext(UserContext);
    const { toRender, setToRender } = useContext(RenderProvider);
    const [isEmpty, setIsEmpty] = useState(false);
    const [allOrders, setAllOrders] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/myOrders`, {
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data === "object"){
                setAllOrders(data.map(order => {
                    return (
                        <AllOrdersTable key={order._id} allOrderProp={order} />
                    )
                }))
                setIsEmpty(false)
            }else {
                setIsEmpty(true)
            }

            setToRender(false)
        })
        .catch(error => console.log(error))
    }, [toRender]);


    return(
        (user.role === "undefined") ?
        <Navigate to='/' />
        :
            <>
                <Row>
                    <Col xs={12} className="mt-1 text-center">
                        <div className="table col-12">
                            <Table className='bg-white' bordered hover>
                                <thead>
                                    <tr>
                                        <th>IMG</th>
                                        <th>PRODUCT NAME</th>
                                        <th>QUANTITY</th>
                                        <th>PRICE</th>
                                        <th>TOTAL AMOUNT</th>
                                        <th>PAID?</th>
                                        <th>STATUS</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>
                                {(isEmpty === false) ? allOrders : <tbody><tr><td>{`You have no orders.`}</td></tr></tbody>}
                            </Table>
                        </div>
                    </Col>
                </Row>
            </>
        )
};