import { useContext, useState, useEffect } from "react";
import { Table, Row, Col,Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import AllOrdersTable from "../components/AllOrdersTable";
import UserContext from "../UserContext";
import RenderProvider from '../RenderContext';



export default function AllOrders(){

    const { user } = useContext(UserContext);
    const { toRender, setToRender } = useContext(RenderProvider);
    const [status, setStatus] = useState("pending");
    const [isEmpty, setIsEmpty] = useState(false)
    const [allOrders, setAllOrders] = useState([]);

    function shipped() {
        setStatus("shipped")
        setToRender(true)
    };

    function pending() {
        setStatus("pending")
        setToRender(true)
    };

    function delivered() {
        setStatus("delivered")
        setToRender(true)
    };

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/allOrders`, {
            method: "POST",
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                status: status
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data === "object"){
                setAllOrders(data.map(order => {
                    return (
                        <AllOrdersTable key={order._id} allOrderProp={order} />
                    )
                }))
                setIsEmpty(false)
            }else {
                setIsEmpty(true)
            }

            setToRender(false)
        })
        .catch(error => console.log(error))
    }, [toRender]);

    return(
        (user.role === "Buyer") ?
        <Navigate to='/' />
        :
            <>
                <Row>
                    <Col xs={12} className="bg-white d-flex mt-2 p-3">
                        <h3>Search for:</h3>
                        <Button className="ms-2" onClick={pending}>Pending</Button>
                        <Button className="mx-2" onClick={shipped}>Shipped</Button>
                        <Button onClick={delivered}>Delivered</Button>
                    </Col>
                    <Col xs={12} className="mt-1 text-center">
                        <div className="table col-12">
                            <Table className='bg-white' bordered hover>
                                <thead>
                                    <tr>
                                        <th>USERNAME</th>
                                        <th>PRODUCT NAME</th>
                                        <th>QUANTITY</th>
                                        <th>TOTAL AMOUNT</th>
                                        <th>ADDRESS</th>
                                        <th>CONTACT NUMBER</th>
                                        <th>PAID?</th>
                                        <th>STATUS</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>
                                {(isEmpty === false) ? allOrders : <tbody><tr><td>{`Theres no ${status} orders.`}</td></tr></tbody>}
                            </Table>
                        </div>
                    </Col>
                </Row>
            </>
        )
};