import { useContext } from 'react';
import { Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Banner from '../components/Banner';
import UserContext from '../UserContext';

export default function Admin(){
    const { user } = useContext(UserContext)

    return(
        (user.role === "Buyer") ?
            <Navigate to='/' />
        :
            <>
                <Row className='mt-2'>
                    <Col className='adminBanner mx-1'>
                        <Banner />
                    </Col>
                </Row>
            </>
    )
};