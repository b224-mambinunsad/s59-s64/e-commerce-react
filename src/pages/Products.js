import { useState, useEffect } from 'react';
import { Row } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import Footer from '../components/Footer';


export default function Products(){
    
    const [products, setProducts] = useState([]);
    const [hotProducts, setHotProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/`)
        .then(res => res.json())
        .then(data => {
            setProducts(data.map(product => {
                return (
                    <ProductCard key={product._id} productProp={product} />
                )
            }))
        })
        .catch(error => console.log(error))
    }, []);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/hotProducts`)
        .then(res => res.json())
        .then(data => {
            setHotProducts(data.map(product => {
                return (
                    <ProductCard key={product._id} productProp={product} />
                )
            }))
        })
        .catch(error => console.log(error))
    }, []);

    return (
        <>
            <h1>Hot Products</h1>
            <Row className='cardRow mt-1'>
            {hotProducts}
            </Row>
            <h1>All Products</h1>
            <Row className='cardRow justify-content-center mt-1' >
                {products}
            </Row>
            
            <Footer />
        </>
    )
};