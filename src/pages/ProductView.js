import { useState, useEffect, useContext } from "react";
import { Card, Button, Row, Col, InputGroup, Form } from 'react-bootstrap';
import { useParams,  } from "react-router-dom"; 
import Swal from 'sweetalert2';
import Star from '../images/star.png';
import ModalOrder from "../components/ModalOrder";
import ModalAddReviews from "../components/ModalAddReviews";
import ProductReviews from "../components/ProductReviews";
import UserContext from '../UserContext';
import RenderContext from '../RenderContext';



export default function ProductView(){
    
    const { productId } = useParams();
    const { user } = useContext(UserContext);
    const { toRender, setToRender } = useContext(RenderContext);

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [img, setImg] = useState("");
    const [price, setPrice] = useState(0);
    const [reviews, setReviews] = useState([]);
    const [reviewsLength, setReviewsLength] = useState("0");
    const [stock, setStock] = useState(0);
    const [sold, setSold] = useState(0);
    const [qty, setQty] = useState(1);
    const [productReviews, setProductReviews] = useState([]);

    const order = {productId, name, img, qty}

    let hasReviews = false
    if(reviewsLength >= 1){
        hasReviews = true
    };

    function addQty(){
        setQty(oldQty => {
            return (oldQty === stock ? oldQty : oldQty + 1)
        })
    };

    function substractQty(){
        setQty(oldQty => {
            return (oldQty ===1 ? oldQty : oldQty - 1)
        })
    };

    const handleAddCart = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/carts/addToCart/${productId}`,{
            method: "POST",
            headers:{
                'Content-Type': 'application/json',
                Authorization: ` Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                quantity: qty
            })
        })
        .then(res=> res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: "Add To Cart",
                    icon: "success",
                    text: "Successfully added to your cart!"
                })

                setToRender(true)
            }else if(data === false){
                Swal.fire({
                    title: "Duplicate Product",
                    icon: "error",
                    text: "This product is already on your cart!"
                })
            }else {
                Swal.fire({
                    title: "Something Went Wrong",
                    icon: "error",
                    text: "Please try again"
                })
            }
        })
        .catch(error => {
            Swal.fire({
                title: "Something Went Wrong",
                icon: "error",
                text: "Please try again"
            })
        })

    };

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/find/${productId}`)
        .then(res => res.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setImg(data.img);
            setPrice(data.price);
            setReviews(data.reviews[0])
            setReviewsLength(data.reviews.length)
            setStock(data.stock);
            setSold(data.sold);

            setProductReviews(data.reviews.map(review => {
                return (
                    <ProductReviews key={review._id} reviewProp={review} />
                )
            }))
            
            setToRender(false);

        })
    },[toRender]);

    return(
        <>
            <Row className='mt-3'>
                    <Col xs={12} lg={6}>
                        <Card border='success' className='singleProd align-items-center justify-content-center'>
                            <Card.Img className='p-3 singleProductImg' src={img} />
                        </Card>
                    </Col>
                    <Col xs={12} lg={6}>
                        <Card border='success' className='singleProd'>
                            <Card.Body className='mt-3'>
                                <Card.Title>{name}</Card.Title>
                                <div className='d-flex'>
                                    <img className='star' src={Star} alt="img" />{(reviews !== undefined ? reviews.rating : 0)}
                                    <span className='mx-3'>|</span>
                                    <span className='reviewsCount'>{reviewsLength} Ratings</span>
                                    <span className='mx-3'>|</span>
                                    <p className='reviewsCount'>{sold} sold</p>
                                 </div>
                                <Card.Text>{description}</Card.Text>
                                <Card.Body>
                                    <div className='d-flex'>
                                        <Card.Text>Price:</Card.Text>
                                        <Card.Title className='ms-3 text-danger'>&#8369;{price}.00</Card.Title>
                                    </div>
                                    <div className='d-flex'>
                                        <Card.Text>Stock:</Card.Text>
                                        <Card.Text className='ms-3'>{stock}</Card.Text>
                                    </div>
                                    <div className='d-flex'>
                                        <Card.Text className='pt-2'>Quantity:</Card.Text>
                                        <InputGroup className="ms-3 qtyInput">
                                            <Button onClick={substractQty} variant="outline-secondary">-</Button>
                                            <Form.Control className='text-center'
                                            placeholder={qty}
                                            value={qty}
                                            onChange={e => setQty(e.target.value)}
                                            disabled
                                            />
                                            <Button onClick={addQty} variant="outline-secondary">+</Button>
                                        </InputGroup>
                                    </div>
                                    <div className='mt-5'>
                                        {(user.role === "Buyer" ?
                                            <>
                                                <Button 
                                                className='btn-addToCart' 
                                                variant='outline-success'
                                                onClick={e => handleAddCart(e)}
                                                >Add To Cart</Button>
                                                <ModalOrder orderProp={order} />
                                            </>
                                        :
                                        (user.role === "Admin" || user.role === "Seller") ?
                                            <>
                                                <Button className='btn-addToCart' variant='danger' disabled>Add To Cart</Button>
                                                <Button className='btn-addToCart ms-1' variant='danger' disabled>Buy Now</Button>
                                            </>
                                        : 
                                            <>
                                                <Button className='btn-addToCart' variant='danger' disabled>Login to Add Cart</Button>
                                                <Button className='btn-addToCart ms-1' variant='danger' disabled>Login to Buy</Button>
                                            </>
                                        )}
                                    </div>
                                </Card.Body>
                            </Card.Body>
                        </Card>
                    </Col>
            </Row>
            <Row className='mt-4'>
                    <Col>
                        <h3>Product Reviews</h3>
                        {(user.role === "Buyer") ?
                            <ModalAddReviews productIdProp={productId} />
                        :
                        (user.role === "Admin" || user.role === "Seller") ?
                            <Button className='my-2' variant='danger' disabled>Add Reviews</Button>
                        :
                            <Button className='my-2' variant='danger' disabled>Login to Add Reviews</Button>
                        }
                        {hasReviews ? productReviews : <p>This product has no reviews</p>}
                    </Col>
            </Row>
        </>
    )
};