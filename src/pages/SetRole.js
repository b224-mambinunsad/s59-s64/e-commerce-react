import { useState, useContext } from 'react';
import { Row, Col, Form, Button} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Banner from '../components/Banner';
import UserContext from '../UserContext';



export default function SetRole(){

    const { user } = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [role, setRole] = useState("");
 
    function updateRole(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/setRole`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                email: email,
                role: role
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: "Set Role Success",
                    icon: "success",
                    text: `You have successfully Set ${email} as ${role}`
                })
                setRole("");
                setEmail("");
            }else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: `Please try again later`
                })
            }
        })
        .catch(error => console.log(error))
    };

    return (
        (user.email !== "admin@mail.com") ?
            <Navigate to='/admin' />
        :
            <>
                <Row className='justify-content-center align-items-center row-setRole p-5 mt-3'>
                    <Col className='d-flex align-items-center justify-content-center' lg={6} >
                        <Banner />
                    </Col>
                    <Col lg={6} className="mt-3">
                        <Form onSubmit={e => updateRole(e)} className='bg-white form-setRole p-4'>
                            <h1 className='mb-3'>Set Role</h1>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control 
                                type="email" 
                                placeholder="Email of user you want to update role"
                                value={email} 
                                onChange={e => setEmail(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="addProductDescription">
                                <Form.Label>Role</Form.Label>
                                <Form.Select 
                                aria-label="Default select example"
                                value={role} 
                                onChange={e => setRole(e.target.value)}
                                >
                                    <option value="">Open this select menu</option>
                                    <option value="Admin">Admin</option>
                                    <option value="Seller">Seller</option>
                                </Form.Select>
                            </Form.Group>
                            <div className='text-center'>
                                {(email !== "" && role !=="") ?
                                    <Button variant="outline-success" type="submit">Submit</Button>
                                :
                                    <Button variant="danger" disabled>Submit</Button>
                                }
                            </div>
                        </Form>
                    </Col>
                </Row>
            </>
        
    )
};