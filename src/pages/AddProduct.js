import { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Banner from '../components/Banner';
import UserContext from '../UserContext';


export default function AddProduct(){
    
    const { user } =useContext(UserContext);
    const navigate = useNavigate();
    
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [category, setCategory] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(1);
    const [img, setImg] = useState("");
    const [isBtnActive, setIsBtnActive] =useState(false);

    function addProduct(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
            method: "POST",
            headers: {
                'Content-Type': "application/json",
                Authorization : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                img: img,
                category: category,
                price: price,
                stock: stock
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: "Successully Add Product",
                    icon: "success",
                    text: `You have successfully added product ${name}`
                })

                setName("");
                setDescription("");
                setCategory("");
                setPrice(0);
                setStock(1);
                setImg("");
                setIsBtnActive(false);

                navigate('/admin/addProduct')
            }else if(data === false){
                Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
            }else {
                Swal.fire({
					title: "Duplicate Product Name",
					icon: "error",
					text: "Product name is already exist please try another product name"
				})
            }
        })
        .catch(error => console.log(error))
    };

    useEffect(() => {
        if(name !== "" && description !== "" && category !== "" && img !== ""){
            setIsBtnActive(true)
        }else{
            setIsBtnActive(false)
        }
    }, [name, description, category, img]);

    return (
        (user.role === "Buyer") ?
            <Navigate to="/" />
        :
            <>
                <Row className='row-addProduct'>
                    <Col className='d-flex align-items-center justify-content-center' lg={6}>
                        {(img !== "" ? <img className='addPrdouctImg' src={img}></img> : <Banner />)}
                    </Col>
                    <Col className='p-2'>
                        <Form onSubmit={e => addProduct(e)} className='bg-white form-addProduct p-3'>
                            <h1>Add Product</h1>
                            <Form.Group className="mb-3" controlId="addProductName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder="Product Name"
                                value={name}
                                onChange={e => setName(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="addProductDescription">
                                <Form.Label>Description</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder="Product Description" 
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="addProductDescription">
                                <Form.Label>Category</Form.Label>
                                <Form.Select 
                                aria-label="Default select example"
                                value={category} 
                                onChange={e => setCategory(e.target.value)}
                                >
                                    <option>Open this select menu</option>
                                    <option value="Mobiles">Mobile</option>
                                    <option value="Men's T-Shirts">Men T-shirt</option>
                                    <option value="Women's Dresses">Women Dress</option>
                                    <option value="Sandals">Sandals</option>
                                    <option value="Men Shoes">Men Shoes</option>
                                    <option value="Water Bottles">Water Bottles</option>
                                    <option value="Men Pants">Men Pants</option>
                                    <option value="Mobile Phone Cases">Mobile Phone Cases</option>
                                    <option value="Headphones">Headphones</option>
                                    <option value="Electrical Parts">Electrical Parts</option>
                                    <option value="Cables">Cables</option>
                                    <option value="Bags">Bags</option>
                                    <option value="Women Shoes">Women Shoes</option>
                                    <option value="Makeups">Makeups</option>
                                    <option value="Women Pants">Women Pants</option>
                                    <option value="Shorts">Shorts</option>
                                    <option value="Babies">Babies</option>
                                    <option value="Toys">Toys</option>
                                    <option value="Helmets">Helmets</option>
                                    <option value="Computer">Computer</option>
                                </Form.Select>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="addProductPrice">
                                <Form.Label>Price</Form.Label>
                                <Form.Control 
                                type="number" 
                                placeholder="Product Price"
                                value={price} 
                                onChange={e => setPrice(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="addProductStock">
                                <Form.Label>Stock</Form.Label>
                                <Form.Control 
                                type="number" 
                                placeholder="Product Stocks"
                                value={stock} 
                                onChange={e => setStock(e.target.value)} 
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="addProductImg">
                                <Form.Label>Product Img</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder="Product Img URL" 
                                value={img} 
                                onChange={e => setImg(e.target.value)}
                                />
                            </Form.Group>
                           
                            <div className='text-center'>
                                {(isBtnActive ? 
                                    <Button variant="success" type="submit">Submit</Button>
                                :
                                    <Button variant="danger" disabled >Submit</Button>
                                )}
                            </div>

                        </Form>
                    </Col>
                </Row>
            </>
    )
};