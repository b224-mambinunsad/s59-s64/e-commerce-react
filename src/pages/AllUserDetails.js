import { useEffect, useContext, useState } from 'react';
import { Row, Col, Table } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import AllDetailsTable from '../components/AllDetailsTable';
import UserContext from '../UserContext';



export default function AllUserDetails(){

    const { user } = useContext(UserContext);
    const [allUserDetails, setAllUserDetails] = useState([])

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/users/getAllDetails`, {
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setAllUserDetails(data.map(detail =>{
                return (
                    <AllDetailsTable key={detail._id} detailProp={detail} />
                )
            }))
        })
        .catch(error => console.log(error))
    },[]);


    return(
        (user.email !== "admin@mail.com") ?
            <Navigate to='/admin' />
        :
            <>
                <Row>
                    <Col xs={12}>
                        <div className='table col-12'>
                            <Table wrapperClasses="table-responsive" className='bg-white' bordered hover>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NAME</th>
                                        <th>EMAIL</th>
                                        <th>SHOPNAME</th>
                                        <th>ROLE</th>
                                    </tr>
                                </thead>
                                {allUserDetails}
                            </Table>
                        </div>
                    </Col>
                </Row>
            </>
    )
};