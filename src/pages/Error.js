import { Row, Col, Image, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Banner from '../components/Banner';


export default function Logout(){
    return (
        <Row className='justify-content-center align-items-center row-error p-5 mt-3'>
            <Col className='d-flex align-items-center justify-content-center' lg={6}>
                <Banner />
            </Col>
            <Col lg={6} className="mt-3">
                <div className='bg-white text-center p-5 div-error'>
                    <Image className='img-error' fluid src='https://res.cloudinary.com/dzevfwrwa/image/upload/v1672969876/404-img_mlpuwd.png'/>
                    <p>The page you are looking cannot be found</p>
                    <Button variant='success' as={Link} to='/' >Back to Home</Button>
                </div>
            </Col>
        </Row>
    )
};