import { useState, useEffect, useContext } from "react";
import { Row, Col, Form, Button, Card } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import Banner from '../components/Banner';
import UserContext from "../UserContext";
import RenderContext from "../RenderContext";


export default function Login(){

    const { user, setUser} = useContext(UserContext);
    const { setToRender } = useContext(RenderContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    function userLogin(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access)
                localStorage.setItem('secret', password)
                retrieveUserDetails(data.access)
                setEmail("");
                setPassword("");
                setToRender(true);
                Swal.fire({
                    title: "Login Successfull",
                    icon: "success",
                    text: `Welcome back!`
                })
            }else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: `Kindly check your login details and try again!`
                })
            }
        })
        .catch(error => console.log(error))
    };

    const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
                name: data.name,
                email: data.email,
                shopName: data.shopName,
                role: data.role
			})
		})
	};

    useEffect(() => {
        (email !== "" && password !== "") ?
            setIsActive(true) 
            :
            setIsActive(false)
    }, [email, password]);

    return(
        (user.role === "Admin" || user.role === "Seller") ?
            <Navigate to='/admin' />
        :
        (user.role !== null) ?
            <Navigate to='/' />
        :
            <Row className='justify-content-center align-items-center row-login p-5 mt-3'>
                <Col className='d-flex align-items-center justify-content-center' lg={6} >
                    <Banner />
                </Col>
                <Col lg={6} className="mt-3">
                    <Form onSubmit={e => userLogin(e)} className="bg-white form-login p-4">
                        <Form.Group className="mb-3" controlId="email">
                            <h1>Login</h1>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            placeholder="Password" 
                            value={password}
                            onChange={e => setPassword(e.target.value)}
                            required
                            />
                        </Form.Group>
                        <div className="text-center">
                            { isActive ? 
                                <Button className="btn-login" variant="outline-success" type="submit">Submit</Button> 
                                :
                                <Button className="btn-login" variant="danger" type="submit" disabled>Submit</Button>
                            }
                        </div>
                    </Form>
                    <div className='text-center text-white'>
                    <   p>Don't have an account yet? <Card.Link as={Link} to='/register' className='text-info'>Click Here</Card.Link> to register.</p>
                    </div>
                </Col>
            </Row>
    )
};