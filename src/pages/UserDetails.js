import { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Button} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Banner from '../components/Banner';
import ModalChangePass from '../components/ModalChangePass';
import UserContext from '../UserContext';



export default function UserDetails(){

    const { user } = useContext(UserContext);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [shopName, setShopName] = useState("");
    const [role, setRole] = useState("");

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data === "object"){
                setName(data.name);
                setEmail(data.email);
                setShopName(data.shopName);
                setRole(data.role);
            }
        })
        .catch(error => console.log(error))
    }, []);
 
    

    return (
        (user.id === null) ?
            <Navigate to='/' />
        :
            <>
                <Row className='justify-content-center align-items-center row-setRole p-5 mt-3'>
                    <Col className='d-flex align-items-center justify-content-center' lg={6} >
                        <Banner />
                    </Col>
                    <Col lg={6} className="mt-3">
                        <Form className='bg-white form-setRole p-4'>
                            <h1 className='mb-3'>User Details</h1>
                            <Form.Group className="mb-3" controlId="formUserName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder={name}
                                disabled
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formUserEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder={email}
                                disabled
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder="***************"
                                disabled
                                />
                            </Form.Group>

                            {user.role !== "Buyer" ?
                                <>
                                    <Form.Group className="mb-3" controlId="formShopName">
                                        <Form.Label>Shop Name</Form.Label>
                                        <Form.Control 
                                        type="text" 
                                        placeholder={shopName}
                                        disabled
                                        />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="formRole">
                                        <Form.Label>Role</Form.Label>
                                        <Form.Control 
                                        type="text" 
                                        placeholder={role}
                                        disabled
                                        />
                                    </Form.Group>
                                </>
                            :
                                <Form.Group className="mb-3" controlId="formShopName">
                                    <Form.Label></Form.Label>
                                    <Form.Control 
                                    type="text" 
                                    hidden
                                    />
                                </Form.Group>
                            }

                            <div className='text-center'>
                                <ModalChangePass />
                            </div>
                        </Form>
                    </Col>
                </Row>
            </>
        
    )
};