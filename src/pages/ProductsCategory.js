import { useState, useEffect,useContext } from 'react';
import { Row } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import Footer from '../components/Footer';
import CategoryContext from '../CategoryContext';


export default function ProductsCategory(){
    
    const { category, setCategory } = useContext(CategoryContext);
    const [products, setProducts] = useState([]);
    const [hotProducts, setHotProducts] = useState([]);
    const [isEmpty, setIsEmpty] = useState(true);
 
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/allProductsByCategory`,{
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                category: category
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.length >= 1){
                setProducts(data.map(product => {
                    return (
                        <ProductCard key={product._id} productProp={product} />
                    )
                }))

                setIsEmpty(false)

            }else{
                setIsEmpty(true)
            }
        })
        .catch(error => console.log(error))
    }, []);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/hotProducts`)
        .then(res => res.json())
        .then(data => {
            setHotProducts(data.map(product => {
                return (
                    <ProductCard key={product._id} productProp={product} />
                )
            }))
        })
        .catch(error => console.log(error))
    }, []);

    return (
        <>
            <h1>Hot Products</h1>
            <Row className='cardRow mt-1'>
                {hotProducts}
            </Row>
            <h1>{category}</h1>
            <Row className='cardRow justify-content-center mt-1' >
            {(isEmpty) ? <h1>No Product On Your Desired Category</h1> : products }
            </Row>
            <Footer />
        </>
    )
};