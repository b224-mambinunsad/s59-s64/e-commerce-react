import { useState, useContext } from 'react';
import { Row, Col, Form, Button} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Banner from '../components/Banner';
import UserContext from '../UserContext';



export default function SetShopName(){

    const { user } = useContext(UserContext);
    const [shopName, setShopName] = useState("");
    const navigate = useNavigate();
 
    function update(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/setShopName`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                shopName: shopName
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: "Set Shop Name Success",
                    icon: "success",
                    text: `You have successfully made your shop ${shopName} kindly login again to continue`
                })
                setShopName("");
                navigate('/admin')
            }else if(data === false) {
                Swal.fire({
                    title: "ShopName Already Exist",
                    icon: "error",
                    text: `${shopName} is already exist please choose another shop name`
                })

                
            }else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: `Please try again later`
                })
            }
        })
        .catch(error => console.log(error))
    };

    return (
        (user.shopName !=="undefined") ?
            <Navigate to='/admin' />
        :
            <>
                <Row className='justify-content-center align-items-center row-setRole p-5 mt-3'>
                    <Col className='d-flex align-items-center justify-content-center' lg={6} >
                        <Banner />
                    </Col>
                    <Col lg={6} className="mt-3">
                        <Form onSubmit={e => update(e)} className='bg-white form-setRole p-4'>
                            <h1 className='mb-3'>Set Shop Name</h1>
                            <Form.Group className="mb-3" controlId="formsetShopName">
                                <Form.Label>Shop Name</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder="Your Desired Shop Name"
                                value={shopName} 
                                onChange={e => setShopName(e.target.value)}
                                />
                            </Form.Group>

                            <div className='text-center'>
                                {(shopName !== "") ?
                                    <Button variant="outline-success" type="submit">Submit</Button>
                                :
                                    <Button variant="danger" disabled>Submit</Button>
                                }
                            </div>
                        </Form>
                    </Col>
                </Row>
            </>
        
    )
};