import { useState, useEffect, useContext } from 'react';
import { Table, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import AllProductsTable from '../components/AllProductsTable';
import RenderProvider from '../RenderContext';
import UserContext from '../UserContext';


export default function AllProducts(){

    const { user } = useContext(UserContext);
    const { toRender, setToRender } = useContext(RenderProvider);
    const [allProducts, setAllProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setAllProducts(data.map(product => {
                return (
                    <AllProductsTable key={product._id} productProp={product} />
                )
            }))
            setToRender(false)
        })
        .catch(error => console.log(error))
    }, [toRender]);
    
    return (
        (user.role === "Buyer") ?
            <Navigate to='/' />
        :
            <>
                <Row>
                    <Col xs={12}>
                        <div className='table col-12'>
                            <Table className='bg-white' bordered hover>
                                <thead>
                                    <tr>
                                        <th>IMG</th>
                                        <th>NAME</th>
                                        <th>PRICE</th>
                                        <th>STOCK</th>
                                        <th>SOLD</th>
                                        <th>AVAILABILITY</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>
                                {allProducts}

                            </Table>
                        </div>
                    </Col>
                </Row>
            </>
    )
};