import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import RenderContext from '../RenderContext'

export default function Logout() {
    
    const { unsetUser, setUser } = useContext(UserContext);
    const { setToRender } = useContext(RenderContext);
    unsetUser()

    useEffect(() => {
        setToRender(true)
        setUser({
            id: null,
            name: null,
            email: null,
            shopName: null,
            role: null
        })
        Swal.fire({
            title: "Logout Successful",
            icon: "success",
            text: "You have successfully logout"
        })
    }, [setUser])

    return (
        <Navigate to='/' />
    )
};