import { Row, Col, Form, Button } from 'react-bootstrap';
import Banner from '../components/Banner';


export default function Contact(){
    return (
        <Row className='justify-content-center align-items-center row-contact p-5 mt-3'>
            <Col className='d-flex align-items-center justify-content-center' lg={6}>
                <Banner />
            </Col>
            <Col lg={6} className="mt-3">
            <Form className='bg-white p-4 form-contact'>
                <h1>Contact us</h1>
                 <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" placeholder="name@example.com" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="name@example.com" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Example textarea</Form.Label>
                    <Form.Control as="textarea" rows={3} />
                </Form.Group>
                <div className='text-center'>
                    <Button variant='outline-success' className='btn-contact'>Send</Button>
                </div>
                
            </Form>
            </Col>
        </Row>
    )
};