import { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Button, Card } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import Banner from '../components/Banner';
import UserContext from '../UserContext';



export default function Register() {

    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isAcitve, setIsActive] = useState(false);

    function registerUser(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
                'Content-Type': "application/json",
            },
            body: JSON.stringify({
                name: name,
                email: email,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data === true){
                setName("");
                setEmail("");
                setPassword1("");
                setPassword2("");
                setIsActive(false)
                Swal.fire({
                    title: "Successully Register",
                    icon: "success",
                    text: "You have successfully registered"
                })

                navigate('/login')
            }else if(data === false){
                Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
            }else {
                Swal.fire({
					title: "Email Already Exist",
					icon: "error",
					text: "Email is already exist please try again!"
				})
            }

        })
        .catch(error => console.log(error))
    };

    useEffect(() => {
        if((password1.length > 5 && password2.length > 5) && (password1 === password2)){
            setIsActive(true);
        }else {
            setIsActive(false);
        }
    }, [password1, password2]);

    return (
        (user.role === "Admin") ?
            <Navigate to='/Admin' />
        :
        (user.role === "Buyer") ?
            <Navigate to='/' />
        :
            <Row className='justify-content-center align-items-center row-signup p-5 mt-3'>
                <Col className='d-flex align-items-center justify-content-center' lg={6}>
                    <Banner />
                </Col>
                <Col lg={6} className="mt-3">
                    <Form onSubmit={e => registerUser(e)} className='bg-white form-signup p-4'>
                        <h1>Register</h1>
                        <Form.Group className="form-register mb-3" controlId="userName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control 
                            type="text" 
                            placeholder="Enter Your First Name" 
                            value={name}
                            onChange={e => setName(e.target.value)}
                            required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                            />
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            placeholder="Input Password that Contains 6 character and up" 
                            value={password1}
                            onChange={e => setPassword1(e.target.value)}
                            required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password2">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            placeholder="Confirm Password" 
                            value={password2}
                            onChange={e => setPassword2(e.target.value)}
                            />
                        </Form.Group>

                        <div className='text-center'>
                            {isAcitve ?
                                <Button className='btn-signup' variant="outline-success" type="submit">Sign Up</Button>
                                :
                                <Button className='btn-signup' variant="danger"  disabled>Sign Up</Button>  
                            }
                        </div>
                    </Form>
                    <div className='text-center text-white'>
                        <p>Already have an account? <Card.Link as={Link} to='/login' className='text-info'>Click Here</Card.Link> to log in.</p>
                    </div>
                </Col>
            </Row>
    )
};