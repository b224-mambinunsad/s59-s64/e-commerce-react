import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Star from '../images/star.png';


export default function ProductCard({productProp}){

    const {name, price, reviews, img, sold, _id} = productProp
    return (
            <>
                <Col className='p-0 mt-1 mb-2'lg={2} md={3} xs={6}>
                    <Card className='trendProdCard m-1'>
                        <Card.Link className='text-black text-decoration-none' as={Link} to={`/products/${_id}`}>
                            <Card.Img className='productCardImg' variant="top" alt="img" src={img} />
                            <Card.Body>
                                <Card.Title className=' titleProductCard mb-3'>{name}</Card.Title>
                                <Card.Text className='cardPrice'>&#8369;{price}.00</Card.Text>
                                <div className='d-flex'>
                                    <img className='star' src={Star} alt="img" />{(reviews[0] !== undefined ? reviews[0].rating : 0)}
                                    <span className='reviewsCount'>({reviews.length})</span>
                                    <p className='ms-auto reviewsCount'>{sold} sold</p>
                                </div>
                            </Card.Body>
                        </Card.Link>
                    </Card>
                </Col>
            </>
    )
};