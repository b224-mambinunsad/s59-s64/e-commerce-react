import { useContext } from 'react';
import { Card, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import CategoryContext from '../CategoryContext';

export default function Categories({categoryProp}){

    const {setCategory} = useContext(CategoryContext);
    const {title, img} = categoryProp;
    const navigate = useNavigate();

    const showCategory = () => {
        setCategory(title)
        navigate('/products/category')
    };

    return (
        <Col lg={1} xs={3} md={2} className="text-center p-0 m-2"> 
            <Card className='categoryCard p-1'>
                <Card.Link type='button' onClick={showCategory}  className='text-black text-decoration-none'>
                    <Card.Img className='categoryImg mb-2' variant="top" src={img} />
                    <Card.Text>{title}</Card.Text>
                </Card.Link>
            </Card>
        </Col>
    )
};