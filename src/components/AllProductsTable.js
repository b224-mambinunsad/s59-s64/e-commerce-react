import { useContext } from 'react';
import { Button,Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ModalUpdateProduct from './ModalUpdateProduct';
import Swal from 'sweetalert2';
import RenderContext from '../RenderContext';


export default function AllProductsTable({productProp}){

    const { setToRender }  = useContext(RenderContext);
   
    const { _id, img, name, price, stock, sold, isActive } = productProp;

    const archive = () => {

        fetch(`${process.env.REACT_APP_API_URL}/products/update/${_id}`, {
            method: "PATCH",
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
					title: "Successully Archive Product",
					icon: "success",
					text: `You have successfully archive product ${name}`
				})

                setToRender(true)

            }else{
                Swal.fire({
					title: "Something Went Wrong",
					icon: "error",
					text: `Unexpected error please try again later!`
				})
            }
        })
        .catch(error => console.log(error))
    };

    const unArchive = (e) => {

        fetch(`${process.env.REACT_APP_API_URL}/products/update/${_id}`, {
            method: "PATCH",
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
					title: "Successully Activate Product",
					icon: "success",
					text: `You have successfully Activate product ${name}`
				})

                setToRender(true)

            }else{
                Swal.fire({
					title: "Something Went Wrong",
					icon: "error",
					text: `Unexpected error please try again later!`
				})
            }
        })
        .catch(error => console.log(error))
    };

    return(
        <tbody>
            <tr className="text-center align-items-center">
                <td><img className="tableImg" src={img}></img></td>
                <td><Card.Link as={Link} to={`/products/${_id}`}>{name}</Card.Link></td>
                <td><p>&#8369;{price}</p></td>
                <td><p>{stock}</p></td>
                <td><p>{sold}</p></td>
                <td><p>{(isActive ? "Available" : "Unavailable")}</p></td>
                <td>
                    <div className='d-block'>
                        <ModalUpdateProduct productIdProp={productProp} />
                    </div>
                    <div className='mt-1'>
                        {(isActive) ? 
                            <Button variant='outline-danger' onClick={archive}>Disable</Button>
                        :
                            <Button variant='outline-success' onClick={e => unArchive(e)}>Enable</Button>
                        } 
                    </div>
                </td>

            </tr>
        </tbody>
    )
};