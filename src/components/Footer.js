export default function Footer(){
    return (
        <>
        <div className="footer text-center mt-3 pt-3">
            <p>Copyright &copy; 2023 Earl John Mambinunsad • Web Designer &amp; Developer</p>
        </div>
        </>
    )
};