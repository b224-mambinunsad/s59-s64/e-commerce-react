import { useState, useEffect, useContext } from "react";
import { Form,Button, Modal, Image } from "react-bootstrap";
import Swal from "sweetalert2";
import RenderContext from "../RenderContext";


export default function ModalAddReviews({productIdProp}){

    const { setToRender } = useContext(RenderContext);

    const [img, setImg] = useState("");
    const [productName, setProductName] = useState("");
    const [show, setShow] = useState(false);
    const [rating, setRating] = useState(1);
    const [comment, setComment] =useState("");

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleAddReviews = (e) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/addReview/${productIdProp}`, {
            method: "PATCH",
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                rating: rating,
                comment: comment
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: "Add Review Success",
                    icon: "success",
                    text: `Successfully added a review on product ${productName}`
                })
                setRating(1);
                setComment("");
                setToRender(true);
                handleClose();
            }else{
                Swal.fire({
                    title: "Something Went Wrong",
                    icon: "error",
                    text: "Please try againg later"
                })
            }
        })
        .catch(error => console.log(error))

    };

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/find/${productIdProp}`)
        .then(res => res.json())
        .then(data => {
            if(typeof data === "object"){
                setImg(data.img)
                setProductName(data.name)
            }
        })
        .catch(error => console.log(error))
    },[]);

    return(
        <>
            <Button onClick={handleShow} className='my-2' variant='outline-success'>Add Reviews</Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                <Modal.Title>Add Review</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="text-center bg-success p-3">
                        <Image src={img} fluid />
                        <h3 className="mt-3 text-white">{productName}</h3>
                    </div>
                    <Form onSubmit={e => handleAddReviews(e)}>
                    <Form.Group className="mb-3 mt-3" controlId="formRating">
                                    <Form.Label>Ratings</Form.Label>
                                    <Form.Select 
                                    aria-label="Default select example"
                                    value={rating} 
                                    onChange={e => setRating(e.target.value)}
                                    >
                                        <option value="">Choose from 1 to 5</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </Form.Select>

                                    
                                    <Form.Group className="mb-3" controlId="formComment">
                                        <Form.Label>Comment</Form.Label>
                                        <Form.Control 
                                        as="textarea" 
                                        rows={3} 
                                        placeholder="Your Comments"
                                        value={comment}
                                        onChange={e => setComment(e.target.value)}
                                        />
                                    </Form.Group>

                                    <div className="mt-3">
                                        {(rating !== "" && comment !="") ?
                                            <>
                                            <Button onClick={handleClose}  className="me-2" variant="secondary">Close</Button>
                                            <Button type="submit" variant="outline-success">Add Review</Button>
                                            </>
                                        :
                                            <>
                                            <Button onClick={handleClose}  className="me-2" variant="secondary">Close</Button>
                                            <Button type="submit" variant="danger">Add Review</Button>
                                            </>
                                        }
                                    </div>
                        </Form.Group>
                    </Form>
                </Modal.Body>

            </Modal>
        </>
    )
};