import { Card } from 'react-bootstrap';
import Star from '../images/star.png';


export default function ProductReviews({reviewProp}){

    const { userName, rating, comment } = reviewProp;

    return (
        <Card>
        <Card.Body>
            <Card.Subtitle>User: {userName}</Card.Subtitle>
            <Card.Subtitle className='my-1'>Ratings: <img className='star' alt='img' src={Star}></img>{rating}</Card.Subtitle>
            <Card.Text><strong>Comments: </strong>{comment}</Card.Text>
        </Card.Body>
        </Card>
    )
};