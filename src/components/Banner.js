import Logo from '../images/logo.png';


export default function Banner(){
    return (
        <div className='text-center'>
            <img className='banner-logo' src={Logo} alt='img'/>
            <h1 className='text-white'>Shopping</h1>
            <h3 className='text-white mt-5'>The leading online shopping platform in Philippines</h3>
        </div>
    )
};