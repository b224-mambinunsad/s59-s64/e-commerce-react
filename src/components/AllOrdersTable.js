import { useState, useEffect, useContext } from 'react';
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import ModalUpdateStatus from './ModalUpdateStatus';
import ModalCancelOrder from "../components/ModalCancelOrder";
import UserContext from '../UserContext';



export default function AllOrdersTable({allOrderProp}){

    const { user } = useContext(UserContext);
    const [img, setImg] = useState("");
    const { _id, userName, totalAmount, products, address, contactNo, isPaid, status } = allOrderProp;

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/find/${products[0].productId}`)
        .then(res => res.json())
        .then(data => {
            if(typeof data === "object"){
                setImg(data.img)
            }else {
                Swal.fire({
                    title: "Something Went Wrong",
                    icon: "error",
                    text: "Please try again later!"
                })
            }
        })
        .catch(error =>console.log(error))
    }, []);
    return(
        (user.role === "Buyer") ?
            <tbody>
                <tr className="text-center align-items-center">
                    <td><img className="tableImg" src={img}></img></td>
                    <td><Card.Link as={Link} to={`/products/${products[0].productId}`} >{products[0].productName}</Card.Link></td>
                    <td><p>{products[0].quantity}</p></td>
                    <td><p>&#8369;{products[0].price}</p></td>
                    <td><p>&#8369;{totalAmount}</p></td>
                    <td><p>{(isPaid) ? "Yes": "No" }</p></td>
                    <td>{status}</td>
                    <td>
                        <div>
                            {(status === "pending") ?
                                <ModalCancelOrder idProp={_id}/>
                            :
                                <Button variant='info' disabled>Order Again</Button>
                            }
                        </div>
                    </td>
                    

                </tr>
            </tbody>
        :
            <tbody>
                <tr className="text-center align-items-center">
                    <td><p>{userName}</p></td>
                    <td><p>{products[0].productName}</p></td>
                    <td><p>{products[0].quantity}</p></td>
                    <td><p>&#8369;{totalAmount}</p></td>
                    <td><p>{address}</p></td>
                    <td><p>{contactNo}</p></td>
                    <td>{(isPaid) ? <p>Yes</p> : <p>No</p> }</td>
                    <td><p>{status}</p></td>
                    <td>
                        <ModalUpdateStatus idProp={_id} />
                    </td>
                    

                </tr>
            </tbody>
    )
};