import { useState, useEffect, useContext } from 'react';
import { Modal, Button,Image, Accordion, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Cart from '../images/cart.png'
import CartProduct from './CartProduct';
import RenderContext from '../RenderContext';


export default function ModalViewCart() {

  const { toRender, setToRender } = useContext(RenderContext);

  const [cartLength, setCartLength] = useState("");
  const [cartProducts, setCartProducts] = useState([]);
  const [isEmpty, setIsEmpty] = useState(true);
  const [address, setAddress] = useState("");
  const [contactNo, setContactNo] = useState ("");
  
  
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleCheckOut = (e) => {
    e.preventDefault()
    
    fetch(`${process.env.REACT_APP_API_URL}/carts/orderCart`, {
        method: "POST",
        headers: {
            'Content-Type': "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            address: address,
            contactNo: contactNo
        })
    })
    .then(res => res.json())
    .then(data => {
        if(data === true){
            Swal.fire({
                title: "Successfully Checkout",
                icon: "success",
                text: "You have successfully checkout your cart"
            })
            setToRender(true)
            handleClose()
        }else{
            Swal.fire({
                title: "Something Went Wrong",
                icon: "error",
                text: "Please try again later!"
            })
        }
    })
    .catch(error => console.log(error))
  }

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/carts/`,{
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
        if(data.length >= 1){
            setIsEmpty(false)
            setCartProducts(data.map(product => {
                return(
                    <CartProduct key={product._id} productProp={product} />
                )
            }))
        }else{
            setIsEmpty(true)
            handleClose()

        }

        setCartLength(data.length)
        setToRender(false)
    })
    .catch(error => console.log(error))
  },[toRender]);

  return (
    <>
        {(isEmpty)?
            <>
                <Button disabled variant='light' className="cart">
                    <div className='cart-container'>
                        <Image src={Cart} fluid className='img-cart m-1' />
                        <h1 className='cart-text'>{cartLength}</h1>
                    </div>
                </Button>
            </>
        :
            <>
                <Button variant='light' className="cart" onClick={handleShow}>
                    <div className='cart-container'>
                        <Image src={Cart} fluid className='img-cart m-1' />
                        <h1 className='cart-text'>{cartLength}</h1>
                    </div>
                </Button>
            </>
        }

      <Modal show={show} onHide={handleClose} position="right">
        <Modal.Header closeButton>
          <Modal.Title>Cart</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Accordion >
                {cartProducts}
            </Accordion>
        </Modal.Body>
        <div>
             <Form onSubmit={e => handleCheckOut(e)} className='mx-5 mt-2'>
                <Form.Group className="mb-3" controlId="formAddress">
                    <Form.Control 
                    type="text" 
                    placeholder="Your Address" 
                    value={address}
                    onChange={e => setAddress(e.target.value)}
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formContactNo">
                    <Form.Control 
                    type="text" 
                    placeholder="Your Contact No" 
                    value={contactNo}
                    onChange={e => setContactNo(e.target.value)}
                    />
                </Form.Group>
                <div className='text-center mb-4'>
                    {(address !== "" && contactNo.length > 10) ?
                        <Button variant='outline-success' type='submit'>Check Out</Button>
                    :
                        <Button variant='danger' disabled>Check Out</Button>
                    }
                </div>
            </Form>
        </div>
      </Modal>
    </>
  )
};
