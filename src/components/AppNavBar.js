import { useContext } from 'react';
import { Button, Container, Form, Navbar, Nav, Image, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Logo from '../images/logo.png';
import Search from '../images/search.png';
import Navs from './Navs'
import ModalViewCart from './ModalViewCart';
import UserContext from '../UserContext';


export default function AppNavBar(){

  const { user } = useContext(UserContext);


    return (
      (user.role === "Admin" || user.role === "Seller" ) ?
        <>
           <Navbar expand="lg" className='nav-bar px-lg-5' sticky='top'>
              <Container>
                <Navbar.Brand as={Link} to='/admin'>
                  <Image src={Logo} fluid className="nav-logo" />
                  {(user.role === "Seller" ?
                    <span className='logo-text'>Seller</span>
                  :
                    <span className='logo-text'>Admin</span>
                  )}
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="ms-auto">
                    <Nav.Link as={Link} to="/admin" >Home</Nav.Link>
                    <Nav.Link as={Link} to="/userDetails" >My Details</Nav.Link>
                    {(user.shopName === "undefined") ? 
                      <Nav.Link as={Link} to="/admin/setShopName">Set Shop Name</Nav.Link>
                    :
                      <>
                        <Nav.Link as={Link} to="/admin/addProduct" >Add Products</Nav.Link>
                        <Nav.Link as={Link} to="/admin/allProducts">All Products</Nav.Link>
                        <Nav.Link as={Link} to="/admin/allOrders">All User Orders</Nav.Link>
                      </>
                    }
                    {(user.email !== "admin@mail.com") ?
                      <Nav.Link as={Link} to="/logout" >Logout</Nav.Link>
                    :
                      <>
                        <Nav.Link as={Link} to="/admin/setRole">Set Role</Nav.Link>
                        <Nav.Link as={Link} to="/admin/allUserDetails">All User Details</Nav.Link>
                        <Nav.Link as={Link} to="/logout" >Logout</Nav.Link>
                      </>
                    }
                  </Nav>
                </Navbar.Collapse>
              </Container>
            </Navbar>
        </>
      :
        <> 
          <Navs/>
          <Navbar expand="lg" className='nav-bar px-lg-5' sticky='top'>
            <Container fluid className='d-block'>
              <Row fluid className='w-100'>
                <Col className='d-lg-block d-md-block d-none'>
                  <Navbar.Brand as={Link} to='/'>
                    <Image src={Logo} fluid className="nav-logo" />
                    <span className='logo-text'>Shopping</span>
                  </Navbar.Brand>
                </Col>
                <Col xs={9} md={6} className="d-grid" >
                  <Form className="d-flex">
                      <Form.Control
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                      />
                      <Button variant="outline-success">
                      <Image src={Search} fluid className='img-search' />
                      </Button>
                    </Form>
                </Col>
                <Col xs={3} className="d-flex justify-content-end" >
                    <ModalViewCart />
                </Col>
              </Row>
            </Container>
          </Navbar>
        </>
    )
};