import { useState, useContext } from 'react';
import { Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import RenderContext from '../RenderContext';

export default function ModalCancelOrder({idProp}) {
    
    const { setToRender } = useContext(RenderContext);
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleCancelOrder = () => {

    fetch(`${process.env.REACT_APP_API_URL}/orders/myOrders/${idProp}`,{
        method: "PATCH",
        headers: {
            'Content-Type': "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            status: "cancelled"
        })
    })
    .then(res => res.json())
    .then(data => {
        if(data === true){
            Swal.fire({
                title: "Cancel Order Succss",
                icon: "success",
                text: "You have successfully cancel this order."
            })

            setToRender(true)
            handleClose()
        }else{
            Swal.fire({
                title: "Something Went Wrong",
                icon: "error",
                text: "Please try again later!"
            })
        }
    })
    .catch(error => console.log(error))
  };
  

  return (
    <>
      <Button variant="outline-danger" onClick={handleShow}>
        Cancel Order
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Cancel Order</Modal.Title>
        </Modal.Header>
        <Modal.Body>
           Are you sure you want to cancel this order?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            No
          </Button>
          <Button variant="primary" onClick={handleCancelOrder}>Yes</Button>
        </Modal.Footer>
      </Modal>
    </>
  )
};