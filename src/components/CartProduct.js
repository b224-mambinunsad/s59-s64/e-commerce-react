import { useState, useEffect, useContext } from "react";
import { Accordion,Image,Container, Row, Col,Form,InputGroup, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import RenderContext from "../RenderContext";



export default function CartProduct({productProp}) {

    const { toRender, setToRender } = useContext(RenderContext);
    const { _id, products, totalAmount } = productProp;
    const [img, setImg] = useState("");
    const [num, setNumber] =useState(0);
    const [qty, setQty] = useState(products[0].quantity);
    
    function getRandomInt(max) {
        return Math.floor(Math.random() * max);
    };

    
    const updateAddQty = (e) => {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/carts/update/${_id}`,{
            method: "PATCH",
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                quantity: qty + 1
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                setQty(oldqty => oldqty + 1)
                setToRender(true)
            }
        })
        .catch(error => console.log(error))
    };

    const updateMinusQty = (e) => {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/carts/update/${_id}`,{
            method: "PATCH",
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                quantity: (qty < 2 ? qty : qty - 1)
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                setQty(oldqty => oldqty < 2 ? oldqty : oldqty - 1)
                setToRender(true)
            }
        })
        .catch(error => console.log(error))
    };

    const handleDeleteCart = (e) => {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/carts/delete/${_id}`,{
            method: "DELETE",
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: "Delete Successfull",
                    icon: "success",
                    text: `You have successfully deleted ${products[0].productName}`
                })
                setToRender(true)
            }
        })
        .catch(error => console.log(error))
    };

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/find/${products[0].productId}`)
        .then(res => res.json())
        .then(data => {
            setImg(data.img)
            setNumber(getRandomInt(100))
            setToRender(false)
        })
        .catch(error => console.log(error))
    }, [toRender]);

    return(
            <>
                <Accordion.Header>{products[0].productName}</Accordion.Header>
                <Accordion.Body className="p-0">
                    <div className="d-flex text-center justify-content-center align-items-center">
                        <Button onClick={e => handleDeleteCart(e)} variant="danger" className="btn-delete-cart me-1 py-0">-</Button>
                        <Accordion.Item eventKey={num}>
                            <Container fluid>
                                <Row className="text-center d-flex">
                                    <Col xs={3} className="d-flex flex-center" >
                                        <Image fluid src={img}/>
                                    </Col>
                                    <Col xs={1} className="d-flex justify-content-center align-items-center">
                                        <div>
                                            <p className="m-0 text-mcart " >Price</p>
                                            <h6 className="text-mcart">{products[0].price}</h6>
                                        </div>
                                    </Col>
                                    <Col xs={6} className="d-flex justify-content-center align-items-center col-cart-input">
                                        <Form className="form-cart-qty">
                                            <InputGroup className="input-group-cart" >
                                                <Button onClick={e => updateMinusQty(e)} variant="outline-secondary" id="button-addon1">-</Button>
                                                <Form.Control 
                                                className="cart-input p-0 text-center"
                                                placeholder={qty}
                                                disabled
                                                />
                                                <Button onClick={e => updateAddQty(e)} variant="outline-secondary" id="button-addon1">+</Button>
                                            </InputGroup>
                                        </Form>
                                    </Col>
                                    <Col xs={1} className="d-flex justify-content-center align-items-center ms-2">
                                        <div>
                                            <p className="m-0 text-mcart">Amount</p>
                                            <h5 className="text-mcart">{totalAmount}</h5>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </Accordion.Item>
                    </div>
                </Accordion.Body>
            </>
    )
};