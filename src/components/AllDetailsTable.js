

export default function AllDetailsTable({detailProp}){

    const {_id, name, email, shopName, role} = detailProp;

    return(
        <tbody>
            <tr className="text-center align-items-center">
                <td><p>{_id}</p></td>
                <td><p>{name}</p></td>
                <td><p>{email}</p></td>
                <td><p>{shopName}</p></td>
                <td><p>{role}</p></td>
            </tr>
        </tbody>
    )
};