import {  useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ModalChangePass(){

    const [oldPassword, setOldPassword] = useState("");
    const [newPassword1, setNewPassword1] = useState("");
    const [newPassword2, setNewPassword2] = useState("");
    const [show, setShow] = useState(false);
    
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const secret = localStorage.getItem('secret');

    const update = (e) => {
        e.preventDefault();

        if(secret !== oldPassword){
            Swal.fire({
                title: "Password Error",
                icon: "error",
                text: "Old Password is incorrect please try again!"
            })
        }else if(newPassword1 !== newPassword2 || newPassword1.length < 5){
            Swal.fire({
                title: "Password Error",
                icon: "error",
                text: "New Password is incorrect please try again!"
            })
        }else{
            fetch(`${process.env.REACT_APP_API_URL}/users/updateDetails`, {
                method: "PATCH",
                headers: {
                    'Content-Type': "application/json",
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    password: newPassword1
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data === true){
                    Swal.fire({
                        title: "Successfully Changed",
                        icon: "success",
                        text: "Password Successfully Changed!"
                    })

                    setNewPassword1("");
                    setNewPassword2("");
                    setOldPassword("");

                    handleClose()
                }else{
                    Swal.fire({
                        title: "Something Wnet Wrong",
                        icon: "error",
                        text: "Something went wrong please try againg later"
                    })
                }
            })
            .catch(error => console.log(error))
        }
    };


    return(
        <>
            <Button variant="outline-danger" onClick={handleShow}>
                Change Password
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                <Modal.Title>Change Password</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={e => update(e)}>

                        <Form.Group className="mb-3" controlId="formOldPassword">
                            <Form.Label>Old Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            placeholder="Old Password"
                            value={oldPassword} 
                            onChange={e => setOldPassword(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formNewPassword1">
                            <Form.Label>New Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            placeholder="New Password Contains 6 Character and up"
                            value={newPassword1} 
                            onChange={e => setNewPassword1(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formNewPassword2">
                            <Form.Label>Confirm New Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            placeholder="Confirm Password"
                            value={newPassword2} 
                            onChange={e => setNewPassword2(e.target.value)}
                            />
                        </Form.Group>

                        <div className='text-center'>
                            <Button className='m-2' variant="secondary" onClick={handleClose}>Close</Button>
                            {(newPassword1 !== "") ?
                                <Button variant="outline-success" type='submit'>Change Password</Button>
                            :
                                <Button variant="danger" disabled>Change Password</Button>
                            }
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    )
};