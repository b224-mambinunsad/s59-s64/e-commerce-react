import { useState, useContext } from "react";
import { Button, Modal, Form } from 'react-bootstrap';
import Banner from './Banner';
import Swal from 'sweetalert2';
import RenderContext from "../RenderContext";


export default function ModalUpdateStatus({idProp}) {

    const { setToRender } = useContext(RenderContext);
    const [show, setShow] = useState(false);

    const [status, setStatus] = useState("");

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const updateStatus = (e) => {
      e.preventDefault()

      fetch(`${process.env.REACT_APP_API_URL}/orders/updateOrder/${idProp}`, {
          method: "PATCH",
          headers: {
            'Content-Type': "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            status: status
          })
      })
      .then(res => res.json())
      .then(data =>{
            if(data === true){
              Swal.fire({
              title: "Successully Update Order Status",
              icon: "success",
              text: `You have successfully update order status`
        })
              setToRender(true)

            }else{
                Swal.fire({
                  title: "Something Went Wrong",
                  icon: "error",
                  text: `Unexpected error please try again later!`
                })
            }
        })
        .catch(error => console.log(error))
        };

    return (
      <>
        <Button variant="outline-success" onClick={handleShow}>
            Update Status
        </Button>

        <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        >
            <Modal.Header closeButton>
            <Modal.Title>Update Status</Modal.Title>
            </Modal.Header>
            <Modal.Body className="text-center">
                <div className="modal-updateStatus p-3">
                  <Banner />
                </div>
                <Form onSubmit={e => updateStatus(e)}>
                  <Form.Group className="mb-3 mt-3" controlId="addProductDescription">
                                  <Form.Label>CATEGORY</Form.Label>
                                  <Form.Select 
                                  aria-label="Default select example"
                                  value={status} 
                                  onChange={e => setStatus(e.target.value)}
                                  >
                                      <option value="">Open this select menu</option>
                                      <option value="pending">Pending</option>
                                      <option value="shipped">Shipped</option>
                                      <option value="delivered">Delivered</option>
                                  </Form.Select>

                                  <div className="mt-3">
                                    {(status !== "delivered" && status !== "") ?
                                        <>
                                          <Button onClick={handleClose}  className="me-2" variant="secondary">Close</Button>
                                          <Button type="submit" variant="outline-success">Update</Button>
                                        </>
                                    :
                                        <>
                                          <Button onClick={handleClose}  className="me-2" variant="secondary">Close</Button>
                                          <Button type="submit" variant="danger">Update</Button>
                                        </>
                                    }
                                  </div>
                    </Form.Group>
                </Form>
            </Modal.Body>
            
        </Modal>
      </>
    )
  };