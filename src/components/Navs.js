import { useContext } from 'react';
import { Nav, Dropdown, NavItem, NavLink } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Navs() {

    const { user } = useContext(UserContext);

    return (
        <>
          <Nav className='navs d-flex pt-0'>
            <Nav.Item className='d-flex'>
              <Nav.Link className='p-0 mx-1' as={Link} to='/contact' ><span className='nav-text'>BE A SELLER</span></Nav.Link>
            </Nav.Item>
            <Nav.Item className='d-flex me-auto'>
              <Nav.Link className='p-0 mx-1' as={Link} to='/contact' ><span className='nav-text'>CONTACT US</span></Nav.Link>
            </Nav.Item>
            {
              (user.email !== null) ?
                  <>
                    <Nav.Item className=''>
                      <Nav.Link className='p-0 mx-1' as={Link} to='/userOrders' ><span className='nav-text'>MY ORDER</span></Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link className='p-0 mx-1' as={Link} to='/userDetails' ><span className='nav-text'>MY DETAIL</span></Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link className='p-0 mx-1' as={Link} to='/logout' ><span className='nav-text'>LOGOUT</span></Nav.Link>
                    </Nav.Item>
                  </>
                :
                  <>
                    <Nav.Item className='ms-auto'>
                      <Nav.Link className='p-0 mx-1' as={Link} to='/login' ><span className='nav-text'>LOGIN</span></Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link className="p-0 mx-1" as={Link} to='/register' ><span className='nav-text'>SIGNUP</span></Nav.Link>
                    </Nav.Item>
                  </>
            }
          </Nav>
        </>
    )
};