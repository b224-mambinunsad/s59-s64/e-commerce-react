import { useState, useContext } from "react";
import { Button, Modal, Form, Card, Container, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import RenderProvider from "../RenderContext";


export default function ModalOrder({productIdProp}) {

    const { setToRender } = useContext(RenderProvider);

    const { _id, img, name, description, price, stock } = productIdProp;
    const [show, setShow] = useState(false);
  
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [updatedImg, setUpdatedImg] = useState(img);
    const [updatedName, setUpdatedName] = useState(name);
    const [updatedDescription, setUpdatedDescription] = useState(description);
    const [updatedPrice, setUpdatedPrice] = useState(price);
    const [updatedStock, setUpdatedStock] = useState(stock);

    const updateOrder = (e) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/update/${_id}`, {
            method: "PATCH",
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                img: updatedImg,
                name: updatedName,
                description: updatedDescription,
                price: updatedPrice,
                stock: updatedStock
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
					title: "Successully Update Product",
					icon: "success",
					text: `You have successfully updated product ${updatedName}`
				})

                setToRender(true)

            }else{
                Swal.fire({
					title: "Something Went Wrong",
					icon: "error",
					text: `Unexpected error please try again later!`
				})
            }
        })
        .catch(error => console.log(error))
    };

    return (
      <>
        <Button variant='outline-success' onClick={handleShow} >Update</Button>
        
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
            <Modal.Title>Update Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Row>
                        <Col xs={12}>
                            {(img !== updatedImg ?
                                <Card.Img src={updatedImg}></Card.Img>
                            :
                                <Card.Img src={img}></Card.Img>
                            )}
                        </Col>
                        <Col>
                        <Form onSubmit={e => updateOrder(e)}>
                            <Form.Group className="mb-3" controlId="productName">
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder={updatedName} 
                                value={updatedName}
                                onChange={e => setUpdatedName(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="productImg">
                                <Form.Label>Image</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder={updatedImg} 
                                value={updatedImg}
                                onChange={e => setUpdatedImg(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="productDescription">
                                <Form.Label>Description</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder={updatedDescription} 
                                value={updatedDescription}
                                onChange={e => setUpdatedDescription(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="productPrice">
                                <Form.Label>Description</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder={updatedPrice} 
                                value={updatedPrice}
                                onChange={e => setUpdatedPrice(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="productStock">
                                <Form.Label>Stock</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder={updatedStock} 
                                value={updatedStock}
                                onChange={e => setUpdatedStock(e.target.value)}
                                />
                            </Form.Group>

                            <div className="text-center">
                                <Button variant="secondary" onClick={handleClose}>
                                    Close
                                </Button>
                                <Button onClick={handleClose} className="ms-2" variant="outline-success" type="submit">Update</Button>
                            </div>
                        </Form>
                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
        </Modal>
      </>
    )
  };